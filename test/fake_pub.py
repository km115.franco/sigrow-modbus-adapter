import redis
from json import dumps
from time import sleep, ctime
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)

device_ids = [345, 12, 5, 546, 56785, 1234, 4, 7, 878, 4356]
data = [12.345, 123.12, 123.12, 456.23, 786.345]


def stream(id):
    red.publish('decoded', dumps({
        "id": id,
        "remote_id": data[0],
        "central_id": data[0],
        "batt": 10,
        "rssi": 1,
        "tsample": 423,
        "par": 153,
        "temp": 173,
        "humid": 183,
        "soil_hum": 923,
        "soil_temp": 1.23,
        "soil_ec": 1.23,
    }))


if __name__ == "__main__":
    while True:
        for id in device_ids:
            stream(id)
            sleep(3)
