import redis
from time import sleep, ctime
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)


def stream():
    red.publish('time', ctime())


if __name__ == "__main__":
    while True:
        stream()
        sleep(3)
