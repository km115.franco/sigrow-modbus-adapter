import redis
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)


def listener():
    sub = red.pubsub()
    sub.subscribe('time')
    for message in sub.listen():
        if message is not None and isinstance(message, dict):
            time = message.get('data')
            print(time)


while True:
    listener()
