
def decode_test(start, length, string):
    return int(string[start: start+length], 16)


def print_decoded(message):
    print("type:", decode_test(0, 2, message))
    print("len:", decode_test(2, 4, message))
    print("central_id:", decode_test(6, 4, message))
    print("remote_id:", decode_test(10, 4, message))

    print("batt:", (decode_test(14, 4, message) - 3500)/7)
    print("rssi:", decode_test(18, 4, message))
    print("tsample:", decode_test(22, 4, message))
    print("par:", decode_test(26, 4, message))
    print("temp:", decode_test(30, 4, message))
    print("humid:", decode_test(34, 4, message))
    print("soil_hum:", decode_test(38, 4, message))
    print("soil_temp:", decode_test(42, 4, message))
    print("soil_ec:", decode_test(46, 4, message))
