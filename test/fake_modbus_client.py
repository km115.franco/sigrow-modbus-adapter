import serial

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_rtu

PORT = '/dev/ttyUSB1'

device = 5
print("...get information from:", device)


def main():
    """main"""
    logger = modbus_tk.utils.create_logger("console")

    try:
        # Connect to the slave
        master = modbus_rtu.RtuMaster(
            serial.Serial(port=PORT, baudrate=9600, bytesize=8,
                          parity='N', stopbits=1, xonxoff=0)
        )
        master.set_timeout(5.0)
        master.set_verbose(True)
        logger.info("... serial connected")
        print(":::::::: INFO REGISTERS")
        print("... get count")
        count = master.execute(1, cst.READ_HOLDING_REGISTERS, 1, 1)
        print("... count:", count)
        print("... get address of devices")
        addresses = master.execute(
            1, cst.READ_HOLDING_REGISTERS, 2, count[0] + 1)
        print("... addresses:", addresses)

        if device in addresses:
            print("... device:", device, "found")
            print("... reading 10 registers of device:", device)
            logger.info(master.execute(
                addresses.index(device)+2, cst.READ_HOLDING_REGISTERS, 0, 10))
        else:
            print("... device:", device, "not found")

        # send some queries
        # logger.info(master.execute(1, cst.READ_COILS, 0, 10))
        # logger.info(master.execute(1, cst.READ_DISCRETE_INPUTS, 0, 8))
        # logger.info(master.execute(1, cst.READ_INPUT_REGISTERS, 100, 3))
        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 100, 12))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_COIL, 7, output_value=1))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_REGISTER, 100, output_value=54))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_COILS, 0, output_value=[1, 1, 0, 1, 1, 0, 1, 1]))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_REGISTERS, 100, output_value=xrange(12)))

    except modbus_tk.modbus.ModbusError as exc:
        logger.error("%s- Code=%d", exc, exc.get_exception_code())


if __name__ == "__main__":
    main()
