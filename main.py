from redis import StrictRedis
from modbus_tk import modbus_rtu
import sys, os

import modbus_tk
import modbus_tk.defines as cst
import serial

from threading import Thread
from json import loads, dumps
from decoder import stream_decoder

serial_port = '/dev/ttyUSB0'

redis = StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)

sensors = {}
count = 0
info_slave = None

echo = True

# Create the server
server = modbus_rtu.RtuServer(serial.Serial(serial_port))


def listener():
    global count
    global server
    global echo

    while True:
        sub = redis.pubsub()
        sub.subscribe('message')
        for message in sub.listen():
            if echo:
                print(message)
            if message is not None and isinstance(message, dict):
                data = message.get('data')
                if data != 1 and "payload" in data:
                    data = loads(data)
                    
                    if data["header"] == 22:
                        data = data["payload"].replace("b'", "")
                        data = stream_decoder(data, 22)
                        id = data["remote_id"]

                        if id in sensors:
                            slave = sensors[id]["slave"]
                            modbus_updater(slave, data)
                            sensors[id] = {**sensors[id], **data}
                        else:
                            sensors[id] = {"address": count,
                                        "slave": modbus_creator(count, data),
                                        **data}
                            count = count+1


def modbus_updater(slave, data):
    slave.set_values('0', 0, data["remote_id"])
    slave.set_values('0', 1, data["central_id"])
    slave.set_values('0', 2, data["batt"])
    slave.set_values('0', 3, data["rssi"])
    slave.set_values('0', 4, data["tsample"])
    slave.set_values('0', 5, data["par"])
    slave.set_values('0', 6, data["temp"])
    slave.set_values('0', 7, data["humid"])
    slave.set_values('0', 8, data["soil_hum"])
    slave.set_values('0', 9, data["soil_temp"])
    slave.set_values('0', 10, data["soil_ec"])


def filter_by_key(object, key, target):
    for id in object.keys():
        if object[id][key] == target:
            return object[id]


def modbus_creator(count, data):
    global info_slave
    slave = server.add_slave(count+2)
    info_slave.set_values('0', 1, count)
    info_slave.set_values('0', count+2, data["remote_id"])
    slave.add_block('0', cst.HOLDING_REGISTERS, 0, 100)
    modbus_updater(slave, data)
    return slave


logger = modbus_tk.utils.create_logger(
    name="console", record_format="%(message)s")

logger.info("... redis listener")

redis_listener = Thread(target=listener)
redis_listener.start()


# Info server:
try:
    logger.info("... running...")
    logger.info("... enter 'quit' for closing the server")
    server.start()
    # info device:
    info_slave = server.add_slave(1)
    info_slave.add_block('0', cst.HOLDING_REGISTERS, 0, 100)
    info_slave.set_values('0', 0, 123)  # save something about this device
    while True:
        print("\n")
        cmd = sys.stdin.readline()
        args = cmd.split(' ')

        if cmd.find('quit') == 0:
            print('bye-bye\r\n')
            redis_listener.join()
            break

        if cmd.find('e') == 0:
            print('echo:', not echo)
            echo = not echo

        if cmd.find('clear') == 0:
           os.system('clear')

        if cmd.find('n ') == 0:
            print(":::::::::::::::")
            address = int(args[1])
            result = filter_by_key(sensors, "address", address)

            print("\n:", address)
            print(result)

        if cmd.find('a') == 0:
            for node in sensors.keys():
                values = {key: val for key, val in sensors[node].items() if key !=
                          "slave"}
                print("id:", node, "add:", sensors[node]["address"] + 2)

        if cmd.find('nodes') == 0:
            print(":::::::::::::::")

            for node in sensors.keys():
                values = {key: val for key, val in sensors[node].items() if key !=
                          "slave"}
                print("\n:", node)
                print(values)

        if cmd.find('count') == 0:
            print(":", count)

finally:
    server.stop()
