import os
import sys


def command_listener(redis_listener, filter_by_key, sensors, count, echo):
    global echo
    
    while True:
        print("\n")
        cmd = sys.stdin.readline()
        args = cmd.split(' ')

        if cmd.find('quit') == 0:
            print('bye-bye\r\n')
            redis_listener.join()
            break

        if cmd.find('e') == 0:
            print('echo:', not echo)
            echo = not echo

        if cmd.find('clc') == 0:
            os.system('cls')

        if cmd.find('n ') == 0:
            print(":::::::::::::::")
            address = int(args[1])
            result = filter_by_key(sensors, "address", address)

            print("\n:", address)
            print(result)

        if cmd.find('a') == 0:
            for node in sensors.keys():
                values = {key: val for key, val in sensors[node].items() if key !=
                          "slave"}
                print("id:", node, "add:", sensors[node]["address"] + 2)

        if cmd.find('nodes') == 0:
            print(":::::::::::::::")

            for node in sensors.keys():
                values = {key: val for key, val in sensors[node].items() if key !=
                          "slave"}
                print("\n:", node)
                print(values)

        if cmd.find('count') == 0:
            print(":", count)