
def decoder(start, length, string):
    return int(string[start: start+length], 16)


def stream_decoder(message, header):
    if header==22:
        return {
            "type": decoder(0, 2, message),
            "len": decoder(2, 4, message),
            "central_id": decoder(6, 4, message),
            "remote_id": decoder(10, 4, message),

            "batt": decoder(14, 4, message),
            "rssi": decoder(18, 4, message),
            "tsample": decoder(22, 4, message),
            "par": decoder(26, 4, message),
            "temp": decoder(30, 4, message),
            "humid": decoder(34, 4, message),
            "soil_hum": decoder(38, 4, message),
            "soil_temp": decoder(42, 4, message),
            "soil_ec": decoder(46, 4, message),
        }
