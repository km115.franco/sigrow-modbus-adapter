# Sigrow modbus extension

> Script to decode sigrow data locally

## Requirements

It is required to install `redis` to publish data to be available for other applications like this one

```bash
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis
```

### Redis publisher

A modification for the script bellow was added to the main script to publish information to redis

```python
import redis
from time import sleep, ctime
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)

red.publish("message", dumps({
    "payload": str(message),
    "header": header,
}))

```

It is necessary to include the redis module on the virtual environment:

```bash
/home/pi/sigrow-role-cnv04-mqtt/venv/bin/pip install pyredis
```

### Redis Subscriber

To get the information from other scripts, the code bellow can help as model

```python
import redis
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)


def listener():
    sub = red.pubsub()
    sub.subscribe('message')
    for message in sub.listen():
        if message is not None and isinstance(message, dict):
            result = message.get('data')
            print(result)

listener()
```
