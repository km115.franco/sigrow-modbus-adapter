from json import loads, dumps
from operator import itemgetter

raw = {
    'type': 22,
    'remote_id': 32473,
    'batt': 3669,
    'rssi': 15,
    'tsample': 900,
    'par': 0, 'temp': 1694,
    'humid': 414, 'soil_hum': 0,
    'soil_temp': 1660,
    'soil_ec': 0
}

target = {
    'type': 22,
    'remote_id': 32473,
    'batt': 58,
    'rssi': 15,
    'tsample': 900,
    'par': 0,
    'temp': 16.94,
    'humid': 41.4,
    'soil_hum': 0,
    'soil_temp': 19.64,
    'soil_ec': 0
}


def derived_values(calibrated, calibration):
    # vpd air
    # dew FloatingPointError
    # s pore ec
    def calc_pore_ec(calibrated, calibration):
        soil_ec, soil_hum = itemgetter('soil_ec', 'soil_hum')(calibrated)
        pore_ec_soil_ec_multiplier, pore_ec_soil_hum_a, pore_ec_soil_hum_b = itemgetter(
            "pore_ec_soil_ec_multiplier", "pore_ec_soil_hum_a", "pore_ec_soil_hum_b")(calibration)
        return soil_ec*pore_ec_soil_ec_multiplier / (pore_ec_soil_hum_a*soil_hum+pore_ec_soil_hum_b)

    return {
        **calibrated,
        "pore_ec": calc_pore_ec(calibrated, calibration)
    }


def get_calibrations():
    file = open("calibration_custom.json", "r")
    calibration_custom = loads(file.read())
    file = open("calibration_default.json", "r")
    calibration_default = loads(file.read())
    return calibration_custom, calibration_default


def calibrate(raw):
    target = str(raw['remote_id'])
    calibration_custom, calibration_default = get_calibrations()
    result = raw
    if target in calibration_custom:
        calibration = calibration_custom[target]
    else:
        calibration = calibration_default[22]

    for key in calibration:
        if key in raw:
            try:
                result[key] = (lambda x: eval(calibration[key]))(raw[key])
            except:
                print("...evaluation error")

    else:
        print("Use default configuration")

    return result


print(dumps(raw, indent=4))
print(dumps(calibrate(raw), indent=4))
